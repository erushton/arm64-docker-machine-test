Just a simple project to see if docker-machine worked out of the box with arm64 EC2 instances.

The config.toml I've been using is:

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "arm-runner-manager2"
  url = "https://gitlab.com"
  token = "XXX"
  executor = "docker+machine"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.machine]
    IdleCount = 1
    MaxBuilds = 4
    MachineDriver = "amazonec2"
    MachineName = "erushton-runner-%s"
    MachineOptions = [
      "amazonec2-access-key=YYYYYYYY",
      "amazonec2-secret-key=ZZZZZZZZ",
      "amazonec2-region=us-east-2",
      "amazonec2-vpc-id=vpc-blah",
      "amazonec2-subnet-id=subnet-blah",
      "amazonec2-zone=b",
      "amazonec2-use-private-address=true",
      "amazonec2-security-group=launch-wizard-13",
      "amazonec2-instance-type=t4g.small",
      "amazonec2-ami=ami-0c20a02d97424e248",
    ]
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0
```